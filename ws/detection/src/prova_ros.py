#!/usr/bin/python3.8

import sys

# numpy and scipy
import numpy as np
# OpenCV
import cv2

# Ros libraries
import roslib
import rospy

# Ros Messages
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import String

import os
from matplotlib import pyplot as plt
import mediapipe as mp
from keras import models as ks 
from collections import Counter
import socket
import errno
import time

UDP_IP = "127.0.0.1"
UDP_PORT_READ = 5432
UDP_PORT_WRITE = 5431

mp_holistic = mp.solutions.holistic # Holistic model
mp_drawing = mp.solutions.drawing_utils # Drawing utilities

sock = socket.socket(socket.AF_INET, # Internet
                      socket.SOCK_DGRAM) # UDP

sequence = []
sentence = []
threshold = 0.8
lista = []
rospy.init_node('image_feature', anonymous=True)
pub = rospy.Publisher("gc_sender", String)
rate = rospy.Rate(10)

trovato = ""
actions = np.array(['Acqua', 'Bere', 'Bicchiere', 'Birra alla spina', 'Buon', 'caldo', 'ciao', 'conto', 'Caffe', 'Cappuccino', 'Cibo', 'Cocacola', 'Cracker',
                   'darmi', 'desidera', 'destra', 'dopo', 'Euro', 'forno', 'freddo', 'frizzante', 'Frullatore', 'giorno', 'Gelato',
                    'Gomma da masticare', 'Grazie', 'latte', 'Limone', 'macchiato', 'mi dia', 'mi dica', 'mi dispiace', 'mi scusi', 
                    'non c e', 'Panino', 'Patate', 'Per favore', 'rosso', 'rubinetto', 'saluti', 'succo di frutta', 'The',
                    'Tovagliolo', 'un', 'WC'])

simple_signs = ['Acqua', 'Bicchiere', 'Caffe', 'Cappuccino', 'Cocacola', 'Cracker', 'Euro', 'Frullatore', 'Gelato', 
                'Grazie', 'Limone', 'The', 'Tovagliolo', 'WC', 'Bere', 'Birra alla spina', 'Gomma da masticare', 
                'Cibo', 'Panino', 'Patate']

# array GESTI COMPOSTI:
composed_signs = ['Patate al forno', 'Patate frizzante', 'Bere rosso', 'Acqua frizzante',
                 'Acqua da rubinetto', 'Buon giorno', 'Caffe macchiato', 'Bere succo di frutta']  

# array FRASI:
frasi = ['Buon giorno, desidera?', 'Buon giorno, mi dica?', 'Grazie, ciao', 'Grazie, saluti',
         'Limone caldo', 'Limone freddo', 'Caffe dopo', 'Per favore, un Caffe', 'Si, un Euro', 'Mi scusi, il conto da darmi', 'Caffe non c e mi dispiace', 'WC Patate e a destra', 'Per favore, Bicchiere un']

no_sequences = 180

    # Videos are going to be 30 frames in length
sequence_length = 30

model = ks.load_model('/home/user/ws/src/detection/resources/modello_completo_250epoche.h5') # ---> CABIARE FILE

VERBOSE=False

isPhaseRecog = False
timer = 0

class image_feature:

    def __init__(self):
        # subscribed Topic

	#reduce buffer with topic of camera of robot.
        self.subscriber = rospy.Subscriber("/xtion/rgb/image_raw/compressed",
                 CompressedImage, self.callback,  queue_size = 1, buff_size=2**24)

    def callback(self, ros_data):
        global isPhaseRecog
        global timer
        global sequence
        global sentence
        global lista
        global trovato
        print(str(timer))
        if(rospy.get_param("/sign") == "phase recognition"):
            timer = time.process_time()
            rospy.set_param("/sign","")
            isPhaseRecog = True
        if isPhaseRecog:
            np_arr = np.fromstring(ros_data.data, np.uint8)
            image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR) # OpenCV >= 3.0:    
            cv2.waitKey(1)
            trovato = detection(cv2.flip(image_np,90))
            if (time.process_time() - timer) > 18:
                sock.sendto(trovato.encode('utf-8'), (UDP_IP, UDP_PORT_WRITE))
                sequence = []
                sentence = []
                lista = []
                isPhaseRecog = False
                trovato = ""

def main(args):
    ic = image_feature()

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down ROS Image feature detector module")
    cv2.destroyAllWindows()

def mediapipe_detection(image, model):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # COLOR CONVERSION BGR 2 RGB
    image.flags.writeable = False                  # Image is no longer writeable
    results = model.process(image)                 # Make prediction
    image.flags.writeable = True                   # Image is now writeable 
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR) # COLOR COVERSION RGB 2 BGR
    return image, results

def draw_landmarks(image, results):
    mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS) # Draw pose connections
    #mp_drawing.draw_landmarks(image, results.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS) # Draw left hand connections
    mp_drawing.draw_landmarks(image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS) # Draw right hand connections
def draw_styled_landmarks(image, results):

     #Draw pose connections
    mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS,
                             mp_drawing.DrawingSpec(color=(80,22,10), thickness=2, circle_radius=4), 
                             mp_drawing.DrawingSpec(color=(80,44,121), thickness=2, circle_radius=2)
                             ) 
    # Draw right hand connections  
    mp_drawing.draw_landmarks(image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS, 
                             mp_drawing.DrawingSpec(color=(245,117,66), thickness=2, circle_radius=4), 
                             mp_drawing.DrawingSpec(color=(245,66,230), thickness=2, circle_radius=2)
                             )



def extract_keypoints(results):
    #pose has also visibility
    pose = np.array([[res.x, res.y, res.z, res.visibility] for res in results.pose_landmarks.landmark]).flatten() if results.pose_landmarks else np.zeros(33*4)
    face = np.array([[res.x, res.y, res.z] for res in results.face_landmarks.landmark]).flatten() if results.face_landmarks else np.zeros(468*3)
    #lh = np.array([[res.x, res.y, res.z] for res in results.left_hand_landmarks.landmark]).flatten() if results.left_hand_landmarks else np.zeros(21*3)
    rh = np.array([[res.x, res.y, res.z] for res in results.right_hand_landmarks.landmark]).flatten() if results.right_hand_landmarks else np.zeros(21*3)
    #return np.concatenate([pose, face, lh, rh])
    return np.concatenate([rh, pose])


def extract_distance(results):
    #pose has also visibility
    xb = results.pose_landmarks.landmark[20].x
    xa = results.pose_landmarks.landmark[0].x
    
    yb = results.pose_landmarks.landmark[20].y
    ya = results.pose_landmarks.landmark[0].y
   
    zb = results.pose_landmarks.landmark[20].z
    za = results.pose_landmarks.landmark[0].z
    distance = math.sqrt(((xb - xa)**2+(yb-ya)**2)) if results.pose_landmarks else 0
    #return np.concatenate([pose, face, lh, rh])
    return distance


def return_sentence(results):
    
    frasi_trovate = []
    segni_composti_trovati = []
    segni_semplici_trovati = []
    for result in results:
        word = result[0]
        #Frasi:
        for x in frasi:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                frasi_trovate.append(tupla)
        # Segni composti:
        for x in composed_signs:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                segni_composti_trovati.append(tupla)
        # Segni semplici:
        for x in simple_signs:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                segni_semplici_trovati.append(tupla)
    
    
    frasi_trovate = list(set(frasi_trovate))
    print('\nFRASI TROVATE:\n', frasi_trovate)
    
    segni_composti_trovati = list(set(segni_composti_trovati))
    print('\nSEGNI COMPOSTI TROVATI:\n', segni_composti_trovati)

    segni_semplici_trovati = list(set(segni_semplici_trovati))
    print('\nSEGNI SEMPLICI TROVATI:\n', segni_semplici_trovati)

    lista_frasi = []
    for result in frasi_trovate:
        lista_frasi.append(result[0])
    lista_frasi = list(set(lista_frasi))
    print('\nlista frasi: \n', lista_frasi) 
    
    lista_stringhe = []
    for x in frasi_trovate:
        lista_stringhe.append(x[0])
    lista_stringhe = list(set(lista_stringhe))
    
    lista_finale = []
    for x in lista_stringhe:
        count = 0
        for y in frasi_trovate:
            if x == y[0]:
                count = count + y[1]
        tupla = (x, count)
        lista_finale.append(tupla)
    frasi_trovate = lista_finale
    print(frasi_trovate)
    
    lista_finale = frasi_trovate+segni_composti_trovati+segni_semplici_trovati
    print('\nLISTA FINALE:\n', lista_finale)
    
    num = []
    for x in lista_finale:
        mass = x[1]
        num.append(mass)
    print(num)   
    num_max = max(num)
    print(num_max)

    for x in lista_finale:
        if x[1] == num_max:
            trovato = x[0]

    if trovato == 'Limone caldo':
        trovato = 'Latte caldo'
    if trovato == 'Limone freddo':
        trovato = 'Latte freddo'
    if trovato == 'Buon giorno':
        trovato = 'Buongiorno'
    if trovato == 'WC Patate e a destra':
        trovato = 'WC uomini e a destra'
    if trovato == 'Per favore, Bicchiere un':
        trovato = 'Per favore, bicchiere uno'
    if trovato == 'Buon giorno, desidera?':
        trovato = 'Buongiorno, desidera?'
    if trovato == 'Buon giorno, mi dica?':
        trovato = 'Buongiorno, mi dica?'
    if trovato == 'Per favore, un Caffe':
        trovato = 'Per favore, un caffe'
    if trovato == 'Si, un Euro':
        trovato = 'Si, un euro'
    if trovato == 'Patate frizzante':
        trovato = 'Patate fritte'
    if trovato == 'Bere rosso':
        trovato = 'Vino rosso'
    if trovato == 'Bere succo di frutta':
        trovato = 'Succo di frutta'

    print(trovato)
    return trovato



def detection(frame):

    global sentence 
    global sequence
    global lista
    with mp_holistic.Holistic(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:


            image, results = mediapipe_detection(frame, holistic)
            draw_styled_landmarks(image, results)
        
        
            # Draw landmarks
            #draw_styled_landmarks(image, results)
        
            # 2. Prediction logic
            keypoints = extract_keypoints(results)
            #         sequence.insert(0,keypoints)
            #         sequence = sequence[:30]
            sequence.append(keypoints)
            sequence = sequence[-30:]
        
            if len(sequence) == 30:
                res = model.predict(np.expand_dims(sequence, axis=0))[0]
                #print(actions[np.argmax(res)])
            
            
            #3. Viz logic
                if res[np.argmax(res)] > threshold: 
                    if len(sentence) > 0: 
                        if actions[np.argmax(res)] != sentence[-1]:
                            if results.right_hand_landmarks is not None:
                                sentence.append(actions[np.argmax(res)])
                    else:
                        sentence.append(actions[np.argmax(res)])

                if len(sentence) > 1: 
                    sentence = sentence[-1:]

                # Viz probabilities
                #image = prob_viz(res, actions, image, colors)
            if(len(sentence) > 0):

                lista.append(sentence[0])
            cv2.rectangle(image, (0,0), (640, 40), (245, 117, 16), -1)
            cv2.putText(image, ' '.join(sentence), (3,30), 
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
        
            # Show to screen
            #cv2.imshow('OpenCV Feed', image)
    data = Counter(lista)
    if(len(data.most_common(7)) > 0):
    	print(data.most_common(7))
    	data_most_common = data.most_common(7)
    	global trovato
    	trovato = return_sentence(data_most_common)
    	print(trovato)
    return trovato


if __name__ == '__main__':
    main(sys.argv)