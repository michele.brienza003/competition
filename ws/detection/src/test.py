#!/usr/bin/python3.8
import cv2
import numpy as np
import os
from matplotlib import pyplot as plt
import time
import mediapipe as mp
import datetime
import tensorflow as tf 
from sklearn.metrics import multilabel_confusion_matrix, accuracy_score
import tensorflow as tf
from collections import Counter

mp_holistic = mp.solutions.holistic # Holistic model
mp_drawing = mp.solutions.drawing_utils # Drawing utilities

def ciao():
    print('hello world')

def mediapipe_detection(image, model):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # COLOR CONVERSION BGR 2 RGB
    image.flags.writeable = False                  # Image is no longer writeable
    results = model.process(image)                 # Make prediction
    image.flags.writeable = True                   # Image is now writeable 
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR) # COLOR COVERSION RGB 2 BGR
    return image, results

def draw_landmarks(image, results):
    mp_drawing.draw_landmarks(image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS) 

def draw_styled_landmarks(image, results):
       
    mp_drawing.draw_landmarks(image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS, 
                             mp_drawing.DrawingSpec(color=(245,117,66), thickness=2, circle_radius=4), 
                             mp_drawing.DrawingSpec(color=(245,66,230), thickness=2, circle_radius=2)
                             ) 

def extract_keypoints(results):
    rh = np.array([[res.x, res.y, res.z] for res in results.right_hand_landmarks.landmark]).flatten() if results.right_hand_landmarks else np.zeros(21*3)
    return rh

# Path for exported data, numpy arrays
DATA_PATH = os.path.join('GestiNoManoSinistra') 

# Actions that we try to detect
actions = np.array(['acqua', 'bere', 'bicchiere', 'rubinetto', 'birra alla spina', 'buon', 'caldo', 'cappuccino', 'ciao', 'caffe',
                    'cocacola', 'conto', 'cracker', 'desidera', 'destra', 'dica', 'dopo', 'euro', 'forno', 'freddo',
                    'fritte_frizzante', 'gelato', 'gomma da masticare', 'grazie', 'latte', 'limone', 'macchiato',
                    'mangiare', 'mi dia', 'mi dispiace', 'non', 'panino', 'patate', 'per favore', 'saluti',
                    'scusi', 'succo_di_frutta', 'tea', 'un', 'uomini', 'vino_rosso', 'wc'])
#caffe frullatore mancano
#actions = np.array(['grazie', 'tovagliolo'])
# Thirty videos worth of data
no_sequences = 180

# Videos are going to be 30 frames in length
sequence_length = 30

model = tf.keras.models.load_model('/home/rocchina/tiago_public_ws/src/detection/resources/segni_solo_mano_destra.h5') # ---> CABIARE FILE

colors = [(245,117,16), (117,245,16), (16,117,245), (126,227,245), (126,127,245), (117,245,16), (16,117,245), (126,227,245), (126,127,245), (245,117,16), (117,245,16), (16,117,245), (126,227,245), (126,127,245), (117,245,16), (16,117,245), (126,227,245)]
def prob_viz(res, actions, input_frame, colors):
    output_frame = input_frame.copy()
        
    return output_frame



# 1. New detection variables
sequence = []
sentence = []
threshold = 0.8
lista = []

cap = cv2.VideoCapture('/home/rocchina/tiago_public_ws/src/detection/src/scusi_mi_dia_il_conto.avi')
# Set mediapipe model 
with mp_holistic.Holistic(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:
    while True:

        # Read feed
        ret, frame = cap.read()
        if ret == False:
            break
        # Make detections
        image, results = mediapipe_detection(frame, holistic)
        
        
        # Draw landmarks
        draw_styled_landmarks(image, results)
        
        # 2. Prediction logic
        keypoints = extract_keypoints(results)
#         sequence.insert(0,keypoints)
#         sequence = sequence[:30]
        sequence.append(keypoints)
        sequence = sequence[-30:]
        
        if len(sequence) == 30:
            res = model.predict(np.expand_dims(sequence, axis=0))[0]
            #print(actions[np.argmax(res)])
            
            
        #3. Viz logic
            if res[np.argmax(res)] > threshold: 
                if len(sentence) > 0: 
                    if actions[np.argmax(res)] != sentence[-1]:
                        if results.right_hand_landmarks is not None:
                            sentence.append(actions[np.argmax(res)])
                else:
                    sentence.append(actions[np.argmax(res)])

            if len(sentence) > 1: 
                sentence = sentence[-1:]

            # Viz probabilities
            image = prob_viz(res, actions, image, colors)
        if(len(sentence) > 0):

            lista.append(sentence[0])
        cv2.rectangle(image, (0,0), (640, 40), (245, 117, 16), -1)
        cv2.putText(image, ' '.join(sentence), (3,30), 
                       cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
        
        # Show to screen
        cv2.imshow('OpenCV Feed', image)

        # Break gracefully
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break
    data = Counter(lista)
    print(data.most_common(7))
    cap.release()
    cv2.destroyAllWindows()

