# SciRoc Competition 2021 - Unibas Team

## Clone the repository

git clone https://gitlab.com/michele.brienza003/Competition

## Build image

cd competition
docker build -t competition .

## Configure pal_docker_utils

git clone https://github.com/pal-robotics/pal_docker_utils/


- Copy name of image after build.

After configuring pal_docker_utils, you will need to execute the pal_docker.sh script with the name of the image.

In a terminal 

cd pal_docker_utils/scripts
sudo ./pal_docker.sh -it YOUR_DOCKER_IMAGE


- You can read the name of image with command docker image list and copy id of image with name competition

## Start the simulation

roslaunch demo_motions sciroc_ep2_execution.launch

This launch file start simulation in gazebo, open the listener node of message of GUI and the node for execution of sign.



